import java.io.*;
import java.net.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Client {
	JTextArea receivedMessages;
	JTextField nickName;
	String nick;
	JTextField ipAddress;
	String ip;
	JTextField portNum;
	int port;
	JTextField message;
	BufferedReader reader;
	PrintWriter writer;
	Socket socket;
	JFrame frmMenu;
	
	public static void main(String[] args) {
		Client client = new Client();
		client.init();
	}
	
	private void putMessage(String msg) {
		receivedMessages.append(msg);
		receivedMessages.setCaretPosition(receivedMessages.getDocument().getLength());
	}
	
	public void init() {
		frmMenu = new JFrame("Messenger Client");
		JPanel panel = new JPanel();
		
		nickName = new JTextField(15);
		nickName.setText("User");
		
		ipAddress = new JTextField(15);
		ipAddress.setText("127.0.0.1");
		
		portNum = new JTextField(15);
		portNum.setText("4567");
		
		JButton start = new JButton("Start");
		start.addActionListener(new StartButtonListener());
		
		panel.add(nickName);
		panel.add(ipAddress);
		panel.add(portNum);
		panel.add(start);
		
		frmMenu.getContentPane().add(BorderLayout.CENTER, panel);
		frmMenu.setSize(200, 200);
		frmMenu.setVisible(true);
		frmMenu.setResizable(false);
		frmMenu.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
				}
			});
		
	}
	
	public void go() {
		JFrame frame = new JFrame("Messenger Client:" + nick);
		JPanel panel = new JPanel();
		
		receivedMessages = new JTextArea(15,40);
		receivedMessages.setLineWrap(true);
		receivedMessages.setWrapStyleWord(true);
		receivedMessages.setEditable(false);
		
		JScrollPane scroll = new JScrollPane(receivedMessages);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		message = new JTextField(35);
		message.addActionListener(new SendButtonListener());
		JButton send = new JButton("Send");
		send.addActionListener(new SendButtonListener());
		
		panel.add(scroll);
		panel.add(message);
		panel.add(send);
		
		
		Thread tClient = new Thread(new MessageReceiver());
		tClient.start();
		
		frame.getContentPane().add(BorderLayout.CENTER, panel);
		frame.setSize(480, 330);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
				}
			});
	}
	
	private void configureConnection() throws IOException{
			socket = new Socket(ip, port);
			InputStreamReader readerStrm = new InputStreamReader(socket.getInputStream());
			reader = new BufferedReader(readerStrm);
			writer = new PrintWriter(socket.getOutputStream());
	}
	
	public class StartButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			nick = nickName.getText();
			ip = ipAddress.getText();
			port = Integer.parseInt(portNum.getText());
			try {
				configureConnection();
				go();
				frmMenu.setVisible(false);
			}
			catch(IOException ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Server not found", "Error", JOptionPane.INFORMATION_MESSAGE);
				frmMenu.setVisible(true);
			}
		}
	}
	
	public class SendButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				if (!message.getText().equals("")) {
					writer.println(nick + ": " + message.getText());
					writer.flush();
				}
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			message.setText("");;
			message.requestFocus();
		}
	}
	
	public class MessageReceiver implements Runnable {
		public void run() {
			String msg;
			try {
				while ((msg = reader.readLine()) != null) {
					putMessage(msg + "\n");
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
