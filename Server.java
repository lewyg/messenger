import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.*;


public class Server {
	ArrayList<PrintWriter> outputStreams;
	JTextArea log;
	int port;
	long time;
	SimpleDateFormat sdf;
	
	public static void main(String[] args) {
		Server server = new Server(4567);
		server.init();
		server.go();
	}
	
	public Server(int port){
		this.port = port;
		this.sdf = new SimpleDateFormat("HH:mm:ss");
	}
	
	private void putLog(String msg) {
		log.append(msg);
		log.setCaretPosition(log.getDocument().getLength());
	}
	
	public void init() {
		JFrame frame = new JFrame("Messenger Server Port: " + port);
		JPanel panel = new JPanel();
		
		log = new JTextArea(15,43);
		log.setLineWrap(true);
		log.setWrapStyleWord(true);
		log.setEditable(false);
		
		JScrollPane scroll = new JScrollPane(log);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		panel.add(scroll);
		
		frame.getContentPane().add(BorderLayout.CENTER, panel);
		frame.setSize(500, 300);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
				}
			});
		
	}
	
	public void go() {
		outputStreams = new ArrayList<PrintWriter>();
		putLog("start on port: " + port + "\n");
		
		try {
			@SuppressWarnings("resource")
			ServerSocket serverSocket = new ServerSocket(4567);
			while(true) {
				Socket clientSocket = serverSocket.accept();
				PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
				outputStreams.add(writer);
				
				Thread tServer = new Thread(new ClientsHandle(clientSocket));
				tServer.start();
				putLog("get connected on port: " + clientSocket.getPort() + "\n");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendToEveryone(String msg) {
		Iterator<PrintWriter> it = outputStreams.iterator();
		while(it.hasNext()) {
			try {
				PrintWriter writer = (PrintWriter) it.next();
				writer.println(msg);
				writer.flush();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public class ClientsHandle implements Runnable {
		BufferedReader reader;
		Socket socket;
		
		public ClientsHandle(Socket clientSocket) {
			try {
				socket = clientSocket;
				InputStreamReader isReader = new InputStreamReader(socket.getInputStream());
				reader = new BufferedReader(isReader);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		public void run() {
			String msg;
			try {
				while ((msg = reader.readLine()) != null) {
					time = new Date().getTime();
					msg = "[" + sdf.format(time) + "] " + msg;
					putLog(msg + "\n");
					sendToEveryone(msg);
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
